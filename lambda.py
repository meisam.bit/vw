import json, boto3
from datetime import date

dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('vwdb')
today = date.today()
d1 = today.strftime("%d/%m/%Y")


def lambda_handler(event, context):
    print("Meisam Sharahi - Serverless Lambda Python Project")
    timestamp={'date':d1}
    res=event.copy()
    for key,value in timestamp.items():
        res[key]=value
    table.put_item(Item=res)
    return {
        "code":200, "message":"Movie Added Successfully"
    }